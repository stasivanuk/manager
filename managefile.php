<?php
function delTree($dir) 
{ 
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) 
    {
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
}
$dbhost = '';
$dbuser = '';
$dbpass = '';
$dbname = '';
$arr = array();
$defaultdir= '';


$dbconnect = mysqli_connect ($dbhost, $dbuser, $dbpass, $dbname);
if (!$dbconnect)
{
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
$queryid = "SELECT ID FROM `filemanager`";
$queryresultid = mysqli_query($dbconnect, $queryid);
while ($resultItem = mysqli_fetch_array($queryresultid, MYSQLI_NUM))
{
    $ids[]  = $resultItem[0];
}
$querypaths = "SELECT path FROM `filemanager`";
$queryresultpaths = mysqli_query($dbconnect, $querypaths);
while ($resultItem = mysqli_fetch_array($queryresultpaths, MYSQLI_NUM))
{
    $paths[]  = $resultItem[0];
}
    mysqli_query($dbconnect, "SET CHARACTER SET 'utf8'");
        
        if (isset($_GET['delete'])){
            if ($_GET['performid']!=null){
                
            foreach ($_GET['performid'] as $delete_id) {
                foreach($ids as $itemid)
                {
                    if($delete_id==$itemid) 
                    {
                        
                        $query = "SELECT path FROM `filemanager` WHERE ID=$delete_id";
                        $queryresult = mysqli_query($dbconnect, $query);
                        $array = mysqli_fetch_array($queryresult, MYSQLI_NUM);
                        unlink($array[0]);
                        $query = "DELETE FROM `filemanager` WHERE ID=$delete_id";
                        $queryresult = mysqli_query($dbconnect, $query);
                        header("Location: ".$_SERVER['HTTP_REFERER']);
                    }
                    else
                    {
                        $deletepath=$delete_id."/%" ;
                        $query = "DELETE FROM `filemanager` WHERE path LIKE '$deletepath'";
                        $queryresult = mysqli_query($dbconnect, $query);
                        delTree("$delete_id");
                        header("Location: ".$_SERVER['HTTP_REFERER']);
                    }
                }
            };
            }
            else
            {
                echo "Choose item";
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
        }
        else if (isset($_GET['download']))
        {
            if (count($_GET['performid'])==1)
            {
                if(is_dir($_GET['performid'][0]."/")!=true)
                {
                    $download_id=$_GET['performid'][0];
                    $query = "SELECT path FROM `filemanager` WHERE ID=$download_id";
                    $queryresult = mysqli_query($dbconnect, $query);
                    $array = mysqli_fetch_array($queryresult, MYSQLI_NUM);
                    $filename=$array[0];
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename=' . basename($filename));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($filename));
                    readfile($filename);
                    exit;
                }
                else
                {
                    $downloadpath = $_GET['performid'][0]."/%";
                    $query = "SELECT path FROM `filemanager` WHERE path like '$downloadpath'";
                    $queryresult = mysqli_query($dbconnect, $query);
                    while ($resultItem = mysqli_fetch_array($queryresult, MYSQLI_NUM))
                    {
                        $array[]  = $resultItem[0];
                    }
                    if (count($array)==0)
                    {
                        echo "The folder(s) are empty";
                        $back=$_SERVER['HTTP_REFERER'];
                        echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";  
                    }
                }
            }
            else
            {
                $download_arr=[];
                foreach($_GET['performid'] as $download_id) 
                {
                    if(is_dir($download_id."/")!=true)
                    {
                         $query = "SELECT path FROM `filemanager` WHERE ID=$download_id";
                         $queryresult = mysqli_query($dbconnect, $query);
                         $array = mysqli_fetch_array($queryresult, MYSQLI_NUM);
                         array_push($download_arr, $array[0]);
                    }
                    else
                    {
                         $downloadpath = $_GET['performid'][0]."/%";
                         $query = "SELECT path FROM `filemanager` WHERE path like '$downloadpath'";
                         $queryresult = mysqli_query($dbconnect, $query);
                         while ($resultItem = mysqli_fetch_array($queryresult, MYSQLI_NUM))
                         {
                            $poorarray[]  = $resultItem[0];
                         }
                         foreach ($poorarray as $newid)
                         {
                         array_push($download_arr, $newid);
                         }
                    }
                }
                if (count($download_arr)==0)
                {
                    echo "The folder(s) are empty";
                    $back=$_SERVER['HTTP_REFERER'];
                    echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";  
                }
                else
                {
                    $zip = new ZipArchive();
                    $zip_name = time().".zip";
                    if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
                    {
                        $error .= "* Sorry ZIP creation failed at this time";
                        $back=$_SERVER['HTTP_REFERER'];
                        echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
                    }
                    foreach($download_arr as $path)
                    {
                        if(file_exists($path))
                        {
                            $zip->addFile($path, preg_replace('/^\/home\/lolkvymu\/public_html\/uploaded_files\//','', $path));
                        }
                        else 
                        {
                            echo "file doesn`t exist";
                            $back=$_SERVER['HTTP_REFERER'];
                            echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
                            
                        }
                    }
                $zip->close();
                if(file_exists($zip_name))
                {
                    header('Content-type: application/zip');
                    header('Content-Disposition: attachment; filename="'.$zip_name.'"');
                    ob_clean();
                    flush();
                    readfile($zip_name);
                    unlink($zip_name);
                }
                    else {};
                }
            }
        }
                else if (isset($_POST['submit']))
        {
            $target_path = $_POST['path'];
            if (iconv_strlen($target_path)>41)
            {
            $filename = basename($_FILES['file']['name']);
            $target_path = $target_path.basename( $_FILES['file']['name']);
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path))
            {
	            $query="INSERT INTO `filemanager` (`name`,`path`) VALUES ('$filename','$target_path')";
	            $queryResult = mysqli_query($dbconnect, $query);
	
	            header("Location: ".$_SERVER['HTTP_REFERER']);
            }
            else
            {
                echo "There was an error uploading the file, please try again!";
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
            }
            else
            {
                echo "Error";
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
        }
        else if (isset($_POST['create']))
        {
            if (preg_match("^[\w]", $_POST['foldername']))
            {
                echo "Invalid Characters!";
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
            else if(in_array($_POST['foldername'], scandir($_POST['path'])))
            {
                echo "Folder with such name already exists"; 
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
            else
            {
                mkdir($_POST['path'].$_POST['foldername']);
                header("Location: ".$_SERVER['HTTP_REFERER']);
            };
        }
        else if (isset($_GET['open']))
        {
            if (is_dir($_GET['performid'][0])!=true)
            {
                echo "Choose directory";
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
            else if (count($_GET['performid'])>1)
            {
                echo "It is not possible to open several directories";
                $back=$_SERVER['HTTP_REFERER'];
                echo "<br><a href= ".$back."><button type='button' class='btn'> ".Назад." </button></a>";
            }
            else
            {
            $currentdir=$_GET['performid'];
            $aostd=substr(strrchr($currentdir[0], "/"), 1);
            $previousdir=substr($currentdir[0], 0, iconv_strlen($currentdir[0])-(iconv_strlen($aostd)+1));
            if ($currentdir[0]==$defaultdir)
            {
                header('Location: https://lolkek.net');
            }
            else
            {
                $query = "SELECT * FROM filemanager";
                $queryresult = mysqli_query($dbconnect, $query);
                ?><form action="managefile.php" method="get"><select name="performid[]" size=14 multiple style="width: 800px;">
                <?
                $indir = array_diff(scandir($currentdir[0]), array('..', '.'));
                ?><option value= <? echo $previousdir ?>>...</option><?php
                foreach ($indir as $inside)
                    if (is_dir($currentdir[0]."/".$inside)==true)
                    {
                        ?><option value= <?php echo $currentdir[0]."/".$inside ?>> &#128193;<?php echo $inside ?> </option><?php
                    }
                while ($check = mysqli_fetch_array($queryresult))
                {
                    if ($currentdir[0]."/" == substr($check['path'], 0, iconv_strlen($check['path'])-(iconv_strlen($check['name']))))
                    {
                        ?><option value= <?php echo $check['ID'] ?>> <?php echo $check['name'] ?> </option><?php
                    };
                }
               
            }
            ?>   
            <br /><input type ="submit" name="delete" value="Удалить" style="position: absolute; top: 305px; left: 192px;"/>
            <input type ="submit" name="download" value="Скачать" style="position: absolute; top: 305px; left: 120px;"/>
            <input type ="submit" name="open" value="Открыть папку" style="position: absolute; top: 305px; left: 8px;"/></form>
            <form action = "managefile.php" method="post"
            enctype="multipart/form-data">
            <input type ="text" name="foldername" pattern="^[\w]+$" value="" placeholder="Латинские буквы" style="position: absolute; top: 275px; left: 8px;">
            <input type ="submit" name="create" value="Создать папку" style="position: absolute; top: 275px; left: 188px;">
            <input type='hidden' name="path" value="<?php echo $currentdir[0]."/"; ?>">
            <input type="file" name="file" id="file" value="Choose file" style="position: absolute; top: 335px; left: 8px;" />
            <br />
            <input type="submit" name="submit" value="Загрузить" style="position: absolute; top: 365px; left: 8px;" />
            </form>
            <script>
            document.querySelector('select').ondblclick = function (_e)
            {
                this.selectedIndex = _e.target.index;
                document.querySelector('input[type=submit][name=open]').click();
            };
            </script>
            <?php
            }
        }
mysqli_close($dbconnect);
?>